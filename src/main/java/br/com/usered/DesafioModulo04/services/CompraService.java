package br.com.usered.DesafioModulo04.services;

import br.com.usered.DesafioModulo04.dtos.ClienteDTO;
import br.com.usered.DesafioModulo04.dtos.CompraDTO;
import br.com.usered.DesafioModulo04.dtos.ProdutoDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class CompraService {
    @Autowired
    private ProdutoService produtoService;
    @Autowired
    private ClienteService clienteService;

    private List<CompraDTO> compras = new ArrayList<>();




    public void cadastrarCompra(String cpf, ProdutoDTO produtoDTO) {
        for (CompraDTO compra : compras) {
            if (compra.getCpf().equals(cpf)) ;
            compra.getProduto().add(produtoDTO);

        }
    }


    //Metodo Mostrar Compras
    public List<CompraDTO> mostrarCompras() {
        return compras;
    }

    //Metodo Mostrar Lista de compra por cliente
    public CompraDTO localizarCompraCliente(String cpf) throws Exception {
        for (CompraDTO compra : compras) {
            if (compra.getCpf().equals(cpf)) ;
            return compra;
        }
        throw new RuntimeException("O cliente não tem nenhuma comprar");
    }


}