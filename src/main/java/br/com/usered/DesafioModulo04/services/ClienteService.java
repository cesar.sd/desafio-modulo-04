package br.com.usered.DesafioModulo04.services;


import br.com.usered.DesafioModulo04.dtos.ClienteDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ClienteService {




    private List<ClienteDTO> clientes = new ArrayList<>();

    //Metodo cadastrar Cliente
    public ClienteDTO cadastrarCliente(ClienteDTO clienteDTO) throws Exception{
        verificarCliente(clienteDTO.getCpf());
        clientes.add(clienteDTO);
        return clienteDTO;
    }

     //Metodo Verificar Cliente
    public void verificarCliente(String cpf) throws Exception{
        for (ClienteDTO cliente: clientes){
            if(cliente.getCpf().equals(cpf)){
                throw new RuntimeException("Cliente já existe ");
            }
        }

    }

    //Metodo Localizar  Cliente
    public ClienteDTO LocalizarCliente(String cpf) throws Exception{
        for( ClienteDTO cliente: clientes){
            if (cliente.getCpf().equals(cpf)){
                return cliente;
            }
        }
        throw new RuntimeException ("Cliente não encontrado");
    }

    //Metodo Nostrar lista  Cliente
    public List<ClienteDTO>mostrarClientes(){
    return clientes;
    }

}
