package br.com.usered.DesafioModulo04.services;

import br.com.usered.DesafioModulo04.dtos.ClienteDTO;
import br.com.usered.DesafioModulo04.dtos.ProdutoDTO;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;



@Service
public class ProdutoService {

    private List<ProdutoDTO> produtos = new ArrayList<>();

    //Metodo cadastrar Produto
    public ProdutoDTO cadastrarProduto(ProdutoDTO produtoDTO) throws Exception{
        verificarProduto(produtoDTO.getNome());
        produtos.add(produtoDTO);
        return produtoDTO;
    }

    //Metodo Verificar Produto
    public void verificarProduto(String nome) throws Exception{
        for (ProdutoDTO produto: produtos){
            if(produto.getNome().equals(nome)){
                throw new RuntimeException("O produto já cadastrado!");
            }
        }

    }

    //Metodo Localizar Produto
    public ProdutoDTO localizarProduto(String nome) throws Exception{
        for( ProdutoDTO produto: produtos){
            if (produto.getNome().equals(nome)){
                return produto;
            }
        }
        throw new RuntimeException ("Produto não encontrado");
    }

    //Metodo Nostrar lista Produto
    public List<ProdutoDTO>mostrarProdutos(){
        return this.produtos;
    }

    //BuscarCompra
    public ProdutoDTO BuscarProduto(String nome){
        for(ProdutoDTO produto : produtos){
            if (produto.getNome().equals(nome)){
                return produto;
            }
        }
        throw new RuntimeException("Produto não encontrado");
    }


}
