package br.com.usered.DesafioModulo04.controller;

import br.com.usered.DesafioModulo04.dtos.ClienteDTO;
import br.com.usered.DesafioModulo04.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("/clientes")


public class ClienteController {
    @Autowired
    private ClienteService clienteService;

    @PostMapping
    public ClienteDTO cadastrarCliente(@RequestBody ClienteDTO clienteDTO) throws Exception {
        return clienteService.cadastrarCliente(clienteDTO);
    }


    @GetMapping("/BuscaCpf")
    public ClienteDTO LocalizarCliente(@RequestParam String cpf) throws Exception {
        return clienteService.LocalizarCliente(cpf);
    }

    //mostrarClientes
    @GetMapping
    public List<ClienteDTO> mostrarClientes() {
        return clienteService.mostrarClientes();
    }


}
