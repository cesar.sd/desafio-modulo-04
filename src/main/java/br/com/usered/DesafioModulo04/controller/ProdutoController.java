package br.com.usered.DesafioModulo04.controller;


import br.com.usered.DesafioModulo04.dtos.ProdutoDTO;
import br.com.usered.DesafioModulo04.services.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/produtos")

public class ProdutoController {

    @Autowired
    private ProdutoService produtoService;

    @PostMapping
    public void cadastrarProduto(@RequestBody ProdutoDTO produtoDTO) throws Exception {
        produtoService.cadastrarProduto(produtoDTO);
    }


    @GetMapping
    public List<ProdutoDTO> mostrarProduto() {
        return produtoService.mostrarProdutos();
    }


    @GetMapping("/BuscaNome")
    public ProdutoDTO LocalizarProduto(@RequestParam String nome) throws Exception {
        return produtoService.localizarProduto(nome);
    }


}
