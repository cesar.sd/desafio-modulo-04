package br.com.usered.DesafioModulo04.dtos;

import java.util.ArrayList;
import java.util.List;

public class ProdutoDTO {
    private String nome;
    private double preco;
    private int quantidade;
    private List<ProdutoDTO> Produtos = new ArrayList<>();

    public ProdutoDTO() {

    }


    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }
}
