package br.com.usered.DesafioModulo04.dtos;

import java.util.ArrayList;
import java.util.List;

public class ClienteDTO {


    private String nome;
    private String cpf;
    private String email;
    private List<ClienteDTO> clientes = new ArrayList<>();

    public ClienteDTO() {

    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<ClienteDTO> getClientes() {
        return clientes;
    }

    public void setClientes(List<ClienteDTO> clientes) {
        this.clientes = clientes;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
