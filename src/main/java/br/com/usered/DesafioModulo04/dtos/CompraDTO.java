package br.com.usered.DesafioModulo04.dtos;

import java.util.ArrayList;
import java.util.List;

public class CompraDTO {

    private ClienteDTO cpf;
    private List<ProdutoDTO> produto = new ArrayList<>();

    public CompraDTO() {

    }

    public ClienteDTO getCpf() {
        return cpf;
    }

    public void setCpf(ClienteDTO cpf) {
        this.cpf = cpf;
    }

    public List<ProdutoDTO> getProduto() {
        return produto;
    }

    public void setProduto(List<ProdutoDTO> produto) {
        this.produto = produto;
    }
}
