package br.com.usered.DesafioModulo04;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DesafioModulo04Application {

	public static void main(String[] args) {
		SpringApplication.run(DesafioModulo04Application.class, args);
	}

}
